import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ApiRequestService} from '../../services/api-request.service';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {
  @Input() data;
  busy: Subscription;

  constructor(public activeModal: NgbActiveModal, private apiService: ApiRequestService, private toastrService: ToastrService) { }

  ngOnInit() {
  }

  confirmFn() {
    if(this.data.operation === 'Delete') {
      this.busy = this.apiService.deleteApi([this.data.path, this.data._id].join('/')).subscribe((doc) => {
        this.toastrService.success('Deleted successfully !!');
        this.activeModal.close(true);
      });
    }else {
      this.busy = this.apiService.update([this.data.path, this.data._id].join('/'), this.data.body).subscribe((doc) => {
        this.toastrService.success(this.data.operation +'ed successfully !!');
        this.activeModal.close(true);
      });
    }
  }
}
