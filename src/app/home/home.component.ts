import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ApiRequestService} from "../shared/services/api-request.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private cdRef: ChangeDetectorRef, private apiService: ApiRequestService) {
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

}
