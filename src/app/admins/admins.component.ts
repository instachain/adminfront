import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {ConfirmDialogComponent} from "../shared/components/confirm-dialog/confirm-dialog.component";
import {ApiRequestService} from '../shared/services/api-request.service';
import {Subscription} from 'rxjs/Subscription';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AddAdminComponent} from "./add-admin/add-admin.component";

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {

  data: any = {
    pagination: {
      page: 1,
      pageSize: 25
    },
    list: [],
    busy: Subscription
  };

  constructor(private cdRef: ChangeDetectorRef, private apiService: ApiRequestService, private modalService: NgbModal) {
    this.data.search = function (params) {
      return apiService.get('/admins', params);
    }
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  openStatusDialog(item) {
    const data = Object.assign({}, {_id: item._id, path: '/admins', operation: item.isBlocked ? 'Unblock' : 'Block', body: {isBlocked: !item.isBlocked}});
    const modalRef = this.modalService.open(ConfirmDialogComponent, {windowClass: 'modal-sm ml-auto mr-auto mt-10'});
    modalRef.componentInstance.data = data;

    modalRef.result.then((result) => {
      if (result) {
        Object.assign(item, {isBlocked: !item.isBlocked});
      }
    });
  }

  openDeleteDialog(id, index) {
    const data = Object.assign({}, {_id: id, path: '/admins', operation: 'Delete'});
    const modalRef = this.modalService.open(ConfirmDialogComponent, {windowClass: 'modal-sm ml-auto mr-auto mt-10'});
    modalRef.componentInstance.data = data;

    modalRef.result.then((result) => {
      if (result) {
        this.data.list.splice(index, 1);
        this.data.pagination.total -= 1;
      }
    });
  }

  addDialog() {
    const modalRef = this.modalService.open(AddAdminComponent, {windowClass: 'modal-sm ml-auto mr-auto mt-7'});

    modalRef.result.then((result) => {
      if (result) {
        this.data.list.unshift(result);
        this.data.pagination.total += 1;
      }
    });
  }
}
