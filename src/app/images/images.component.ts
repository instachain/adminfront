import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ApiRequestService} from '../shared/services/api-request.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs/Subscription';
import {ConfirmDialogComponent} from '../shared/components/confirm-dialog/confirm-dialog.component';
import {EditImageComponent} from './edit-image/edit-image.component';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  data: any = {
    pagination: {
      page: 1,
      pageSize: 25
    },
    list: [],
    busy: Subscription
  };

  constructor(private cdRef: ChangeDetectorRef, private apiService: ApiRequestService, private modalService: NgbModal) {
    this.data.search = function (params) {
      return apiService.get('/admin/images', params);
    }
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  openDeleteDialog(id, index) {
    const data = Object.assign({}, {_id: id, path: '/admin/images', operation: 'Delete'});
    const modalRef = this.modalService.open(ConfirmDialogComponent, {windowClass: 'modal-sm ml-auto mr-auto mt-10'});
    modalRef.componentInstance.data = data;

    modalRef.result.then((result) => {
      if (result) {
        this.data.list.splice(index, 1);
        this.data.pagination.total -= 1;
      }
    });
  }

  editDialog(item) {
    const data = Object.assign({}, {path: `/admin/images/${item._id}/replace`, body: Object.assign({}, item)});

    const modalRef = this.modalService.open(EditImageComponent, {windowClass: 'mt-7'});
    modalRef.componentInstance.data = data;

    modalRef.result.then((result) => {
      if (result) {
        Object.assign(item, {url: result.url});
      }
    });
  }
}
