import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  busy: boolean;
  email: string;
  token: string;
  password: string;
  confirmPassword: string;

  constructor(private auth: AuthService, private router: Router, private toastrService: ToastrService, private route: ActivatedRoute) {
    this.route.queryParams
      .subscribe((params: any) => {
        this.email = params.email;
        this.token = params.token;
      });
  }

  ngOnInit() {
  }

  onSubmit() {
    if (this.password !== this.confirmPassword) {
      this.toastrService.error('Password doesn\'t match');
      return false;
    }
    this.busy = true;
    this.auth.resetPassword({email: this.email, newPassword: this.password, passwordResetToken: this.token}).subscribe(
      data => {
        this.busy = false;
        this.toastrService.success('Password has been reset successfully!! ');
        this.router.navigateByUrl('');
      },
      err => {
        this.busy = false;
      }
    );
  }
}
